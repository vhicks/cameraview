# Team CAMERA #
## MPCS 51031 Project ##

Repo: https://bitbucket.org/vhicks/cameraview    
We have created a list view camera app that allows users to select options to change the image within the row.

### 3 Options ###

* Use built-in Camera
* Pick from Gallery
* Populate with a random image from the web

### Here is a link to the ppt that describes our approach ###
[Team Presentation .ppt](https://docs.google.com/presentation/d/1D2b6MurA9wTFKMvot5-plVOycR7Gz4BUHa_pP3IeUZY/edit?usp=sharing)