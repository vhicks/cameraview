package edu.uchicago.feng.cameraview;

import java.io.Serializable;

/**
 * Created by fengziyi on 5/15/17.
 */

public class MyImage implements Serializable{
    private int mId;
    private String mTitle, mDescription;
    private byte[] mImage;

    public MyImage() {

    }

    public MyImage(int id, String title, String description, byte[] image) {
        mId = id;
        mTitle = title;
        mDescription = description;
        mImage = image;
    }

    public int getId() {
        return mId;
    }
    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }
    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }
    public void setDescription(String description) {
        mDescription = description;
    }

    public byte[] getImage() {
        return mImage;
    }
    public void setImage(byte[] image) {
        mImage = image;
    }
}
