package edu.uchicago.feng.cameraview;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Created by fengziyi on 5/15/17.
 */

public class ImageArrayAdapter extends ArrayAdapter<MyImage> {
    private Context context;
    private int layoutResourceId;
    private ArrayList<MyImage> data = new ArrayList<MyImage>();

    public ImageArrayAdapter(Context context, int layoutResourceId, ArrayList<MyImage> data) {
        super(context, layoutResourceId, data);

        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ImageHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ImageHolder();
            holder.textView1 = (TextView)row.findViewById(R.id.textView1);
            holder.textView2 = (TextView)row.findViewById(R.id.textView2);
            holder.imageView = (ImageView)row.findViewById(R.id.imageView);
            row.setTag(holder);
        }
        else
        {
            holder = (ImageHolder)row.getTag();
        }

        MyImage picture = data.get(position);
        holder.textView1.setText(picture.getTitle());
        holder.textView2.setText(picture.getDescription());

        //convert byte to bitmap take from contact class
        byte[] outImage=picture.getImage();
        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        holder.imageView.setImageBitmap(theImage);
        return row;
    }

    static class ImageHolder
    {
        TextView textView1;
        TextView textView2;
        ImageView imageView;
    }
}
