package edu.uchicago.feng.cameraview;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fengziyi on 5/15/17.
 */

public class ImageDbAdapter {
    //these are the column names
    public static final String COL_ID = "_id";
    public static final String COL_TITLE = "title";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_IMAGE = "path";
    //these are the corresponding indices
    public static final int INDEX_ID = 0;
    public static final int INDEX_TITLE = INDEX_ID + 1;
    public static final int INDEX_DESCRIPTION = INDEX_ID + 2;
    public static final int INDEX_IMAGE = INDEX_ID + 3;
    //used for logging
    private static final String TAG = "ImageDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private static final String DATABASE_NAME = "dba_images";
    private static final String TABLE_NAME = "tbl_images";
    private static final int DATABASE_VERSION = 2;
    private final Context mCtx;
    //SQL statement used to create the database
    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
                    COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_TITLE + " TEXT, " +
                    COL_DESCRIPTION + " TEXT, " +
                    COL_IMAGE + " BLOB );";

    public ImageDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }
    //open
    public void open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
    }
    //close
    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    //CREATE
    //note that the id will be created for you automatically
    public void createImage(String title, String description, byte[] image) {
        System.out.println("In createImage(): " + title);
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, title);
        values.put(COL_DESCRIPTION, description);
        values.put(COL_IMAGE, image);
        mDb.insert(TABLE_NAME, null, values);
    }
    //overloaded to take a image
    public long createImage(MyImage myImage) {
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, myImage.getTitle());
        values.put(COL_DESCRIPTION, myImage.getDescription());
        values.put(COL_IMAGE, myImage.getImage());
        // Inserting Row
        return mDb.insert(TABLE_NAME, null, values);
    }
    //READ
    public MyImage fetchImageById(int id) {

        Cursor cursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_TITLE,
                        COL_DESCRIPTION, COL_IMAGE}, COL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null
        );
        if (cursor != null)
            cursor.moveToFirst();
        return new MyImage(
                cursor.getInt(INDEX_ID),
                cursor.getString(INDEX_TITLE),
                cursor.getString(INDEX_DESCRIPTION),
                cursor.getBlob(INDEX_IMAGE)
        );
    }

    public List<MyImage> fetchAllImages() {
        List<MyImage> imageList = new ArrayList<MyImage>();

        Cursor mCursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_TITLE,
                        COL_DESCRIPTION, COL_IMAGE},
                null, null, null, null, null
        );
        // looping through all rows and adding to list
        if (mCursor.moveToFirst()) {
            do {
                MyImage myImage = new MyImage();
                myImage.setId(Integer.parseInt(mCursor.getString(0)));
                myImage.setTitle(mCursor.getString(1));
                myImage.setDescription(mCursor.getString(2));
                myImage.setImage(mCursor.getBlob(3));
                // Adding contact to list
                imageList.add(myImage);
            } while (mCursor.moveToNext());
        }
        // return contact list
        return imageList;

    }
    //UPDATE
    public void updateImage(MyImage myImage) {
        ContentValues values = new ContentValues();
        values.put(COL_TITLE, myImage.getTitle());
        values.put(COL_DESCRIPTION, myImage.getDescription());
        values.put(COL_IMAGE, myImage.getImage());
        mDb.update(TABLE_NAME, values,
                COL_ID + "=?", new String[]{String.valueOf(myImage.getId())});
    }
    //DELETE
    public void deleteImageById(int nId) {
        mDb.delete(TABLE_NAME, COL_ID + "=?", new String[]{String.valueOf(nId)});
    }
    public void deleteAllImages() {
        mDb.delete(TABLE_NAME, null, null);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
