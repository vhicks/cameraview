package edu.uchicago.feng.cameraview;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SharedPreferences prefs = null;

    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public final static int GALLERY_IMAGE_ACTIVITY_REQUEST_CODE = 1032;

    private ListView mListView;
    private ImageArrayAdapter mImageArrayAdapter;
    private ImageDbAdapter mDbAdapter;
    private ArrayList<MyImage> data = new ArrayList<MyImage>();

    private ImageView mCurrentImageView;
    private byte[] mCurrentImage;
    private MyImage mOldImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.camera_list_view);
        mListView.setDivider(null);
        mDbAdapter = new ImageDbAdapter(this);
        mDbAdapter.open();

        prefs = getSharedPreferences("edu.uchicago.feng.cameraview", MODE_PRIVATE);

        System.out.println("In onResume(), firstrun: " + prefs.getBoolean("firstrun", true));
        if (prefs.getBoolean("firstrun", true)) {
            mDbAdapter.deleteAllImages();
            mDbAdapter.createImage("Check", "This is Check", getBytes(BitmapFactory.decodeResource(getResources(), R.drawable.icon1)));
            mDbAdapter.createImage("Check", "This is Check", getBytes(BitmapFactory.decodeResource(getResources(), R.drawable.icon1)));
            mDbAdapter.createImage("Phone", "This is Phone", getBytes(BitmapFactory.decodeResource(getResources(), R.drawable.icon2)));
            mDbAdapter.createImage("Star", "This is Star", getBytes(BitmapFactory.decodeResource(getResources(), R.drawable.icon3)));
            mDbAdapter.createImage("Book", "This is Book", getBytes(BitmapFactory.decodeResource(getResources(), R.drawable.icon4)));
            prefs.edit().putBoolean("firstrun", false).commit();
        }

        List<MyImage> imageList = mDbAdapter.fetchAllImages();
        for (MyImage image : imageList) {
            data.add(image);
        }
        mImageArrayAdapter = new ImageArrayAdapter(MainActivity.this, R.layout.content_row, data);
        mListView.setAdapter(mImageArrayAdapter);

        // set lickListener to show a dialog
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                mCurrentImageView = (ImageView) view.findViewById(R.id.imageView);

                ListView modeListView = new ListView(MainActivity.this);
                final String[] modes = new String[] { "Take a New Photo", "Choose from Library", "Random Web Pic" };
                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(MainActivity.this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, modes);
                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();

                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        mOldImage = mImageArrayAdapter.getItem(masterListPosition);

                        // Take a New Photo
                        if(position == 0) {

                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                            }
                        } else if(position == 1) {
                            // Choose from Library
                            Intent intent = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, GALLERY_IMAGE_ACTIVITY_REQUEST_CODE);

                        } else {
                            // Choose Web Pic
                            SimpleTarget target = new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                                    // do something with the bitmap
                                    mCurrentImageView.setImageBitmap(bitmap);
                                    mCurrentImage = getBytes(bitmap);
                                    changeCurrentImage();
                                }
                            };

                            Glide
                                    .with(MainActivity.this)
                                    .load("https://source.unsplash.com/random")
                                    .asBitmap()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .into(target);
                        }
                        Toast.makeText(MainActivity.this, "You Clicked at " + modes[+position], Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Before inserting into database, you need to convert your Bitmap image into byte array first
    // then apply it using database query.
    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    private void changeCurrentImage() {
        if(mCurrentImage != null){
            mOldImage.setImage(mCurrentImage);
            MyImage currentImage = new MyImage(mOldImage.getId(), mOldImage.getTitle(), mOldImage.getDescription(), mOldImage.getImage());
            System.out.println(String.format("Id: %s, title: %s", mOldImage.getId(), mOldImage.getTitle()));
            mDbAdapter.updateImage(currentImage);
            mCurrentImage = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                mCurrentImageView.setImageBitmap(imageBitmap);

                mCurrentImage = getBytes(imageBitmap);
                changeCurrentImage();


            }
        } else if (requestCode == GALLERY_IMAGE_ACTIVITY_REQUEST_CODE){
            if (resultCode == RESULT_OK) {
                Uri targetUri = data.getData();
                //Uri takenPhotoUri = getPhotoFileUri(photoFileName);
                // by this point we have the camera photo on disk
                //Bitmap takenImage = BitmapFactory.decodeFile(targetUri.getPath());
                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
                    mCurrentImageView.setImageBitmap(bitmap);

                    mCurrentImage = getBytes(bitmap);
                    changeCurrentImage();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
    }
        else
        {
            Toast.makeText(MainActivity.this, "Picture Not taken", Toast.LENGTH_LONG);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                finish();
                return true;
            default:
                return false;
        }
    }
}
